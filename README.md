# C4I Use Cases as Jupyter Notebooks

***A collection of Jupyter Notebooks implementing some Use Cases using [ICCLIM](https://github.com/cerfacs-globc/icclim).***


The notebooks in this repository can be executed in a [Climate4Impact v2](https://dev.climate4impact.eu/c4i-frontend/). Please [Sign Up](https://dev.climate4impact.eu/c4i-frontend/signup) to C4I as an ***alpha*** tester to do so!

Follow the descriptions in each notebook to identify the datasets used for the analysis and sarch them in C4I.


